#!/bin/bash

SCRIPT_PATH=`dirname "$0"`; SCRIPT_PATH=`eval "cd \"$SCRIPT_PATH\" && pwd"`

FILE="sync.json"

if [ -e SCRIPT_PATH/"$FILE" ]; then
    echo "Json file found"
else
    echo "Json file not found. Let's do some setup!"
    echo "Which directories would you like to link?"
    read -p "Source directory: " SOURCE_DIR
    read -p "Target directory: " TARGET_DIR
    read -p "Do you want to use automatic overwrite? (Y/n)" OVERWRITE

    if [ $OVERWRITE -eq "Y" ]; then
	OVERWRITE="true"
    else
	OVERWRITE="false"
    fi

    touch SCRIPT_PATH/sync.json

    echo "Writing config file..."

    /bin/cat <<EOM >$FILE
	{
	    "source": "$SOURCE_DIR",
	    "target": "$TARGET_DIR",
	    "overwrites": "$OVERWRITE"
	}
    EOM
fi

echo "Source Dir: $SOURCE_DIR"
